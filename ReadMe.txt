The attached Python file contains the methods, in a messy format, that were used to extract the feature information.
This was then copied onto an .arff file which allowed for processing in W.E.K.A (a tool for machine learning).

If you wish to create other data files note that you should restrict the dimensionality so that it is proportional to the number of images.
Ie - more images allows more dimensionality in the data, fewer images allows less dimensionality.

The datafiles are extracted from the default set of images felt elsewhere in the project repositories.

They contain three techniques: colour histograms, gray histograms, and histogram of oriented gradients.
An equalised technique was used for colour histograms but this performed worse than non-equalising the image.